{-# LANGUAGE OverloadedStrings, MultiWayIf #-}

--import Debug.Trace.Location
import Control.Concurrent (threadDelay)
import Foreign.C.Types
import SDL.Vect
-- import SDL.Renderer 
import qualified SDL
import SDL (($=),get)
import System.Random
import Data.SplitQuaternionForHyperbolicSurface
import qualified Data.SplitQuaternionForHyperbolicSurface as Q
import Data.Monoid
import Data.Maybe
import SDL.Raw.Timer
import Data.Word
import qualified Data.Vector.Storable as V
import System.IO.Unsafe
import Data.Int
import System.Directory
import Data.List

-- fromJustSafe a s = check a (fromJust s)
-- #define fromJust (fromJustSafe assert)


main :: IO ()
main = do
  SDL.initialize [SDL.InitVideo]

  window <- SDL.createWindow "Hyperbolic surface" SDL.defaultWindow { SDL.windowInitialSize = V2 1792 1024 }
  SDL.showWindow window

  screenSurface <- SDL.getWindowSurface window
  --let white = V4 maxBound maxBound maxBound maxBound
  --let bg = V4 0 120 100 256
  --SDL.surfaceFillRect screenSurface Nothing bg
  --SDL.updateWindowSurface window

  ren <- SDL.createRenderer window (-1) SDL.defaultRenderer
  -- ren <- SDL.createRenderer window (-1) $ SDL.RendererConfig SDL.SoftwareRenderer False

  loop ren $ State 0 True 0 (SQ 1 0.00000000000001 0 0) False [] [] (SQ 1 0 0 0) False False 0 False Nothing False (-999999) []
                   False Nothing Nothing Nothing [[]] False False [] Nothing False

  threadDelay 2000000

  SDL.destroyWindow window
  SDL.quit

data State = State { lastt :: Word32, redraw :: Bool, p01 :: Double, tran :: SQ, mousePressed :: Bool, drawOnRen :: [Point V2 Int32]
                   , drawPoints :: [[SQ]] , tranB :: SQ, dumpDrawings :: Bool, loadDrawings :: Bool, scalePnt :: Double, zoom :: Bool, savedTran :: Maybe SQ
                   , projU :: Bool, maxX :: Double, circles :: [(SQ,SQ)], circleDrawing :: Bool
                   , circlePointA :: Maybe (Point V2 Int32), circlePointB :: Maybe (Point V2 Int32)
                   , lastCircle :: Maybe (SQ,SQ), cirCol :: [[(SQ,SQ)]]
                   , centricProjection :: Bool, multTest :: Bool, multTests :: [(SQ,SQ)], lastMultTest :: Maybe (SQ,SQ)
                   , heptaProjection :: Bool
                   }

funTra state = state2 { tran = tr3, tranB = tr4 }
 where
  tr1 = tran  state
  tr2 = tranB state
  tr3 = tr2 * tr1
  tr4 = SQ 1 0 0 0
  state2 = if tranB state == tr4 then state else state { circlePointA = Nothing }

sysFun state
 | dumpDrawings state = do
     a <- randomRIO (2^16,2^32 :: Integer)
     -- writeFile ("dump" ++ show a ++ ".txt") $ show $ (drawPoints state :: [[SQ]])
     writeFile ("dump" ++ show a ++ ".txt") $ show $ ((filter (not.null) (circles state : cirCol state), multTests state, drawPoints state) :: ([[(SQ,SQ)]],[(SQ,SQ)],[[SQ]]))
     return $ state { dumpDrawings = False }
 | loadDrawings state = do
     list <- getDirectoryContents "."
     let l01 = filter (\a -> take 4 a == "dump") list
     let l02 = filter (\a -> (reverse $ take 4 $ reverse a) == ".txt") l01
     if null l02 then return state
                 else do
                   r <- randomRIO (0,length l02-1)
                   a <- readFile $ l02 !! r
                   --return $ state { loadDrawings = False, drawPoints = read a }
                   let (col,mtsts,pnts) = read a
                   return $ state { loadDrawings = False, circles = head col, cirCol = tail col ++ [[]], drawPoints = pnts, multTests = mtsts }
 | otherwise = return state
   

loop ren state0 = do
  events <- map SDL.eventPayload <$> SDL.pollEvents
  let mxxf = max 0.4 ( exp ( max 1 (maxX state0) * 2 - 6 ) / 2 )
  --print mxxf
  let fz01L st = if zoom st then (1/4000) else 20
  let fz01R st = if zoom st then (-(1/4000)) else (-20)
  let fm01L st = if zoom st then (1/4000000) else 20
  let fm01R st = if zoom st then (-(1/4000000)) else (-20)
{-
  let fz02L st = if zoom st then (1/400000) else (1)/10
  let fz02R st = if zoom st then (-(1/400000)) else (-1)/10
-}
  let fz02L st = if zoom st then (1/400000) else (1)/mxxf
  let fz02R st = if zoom st then (-(1/400000)) else (-1)/mxxf
  let ff e = case e of
               SDL.MouseMotionEvent e
                  | SDL.mouseMotionEventState e == [SDL.ButtonLeft] -> Last $ Just $ \st ->
                       case (circleDrawing st) of
                         (False) -> st { drawOnRen = SDL.mouseMotionEventPos e : drawOnRen st }
                         (True)  -> st { circlePointB = Just $ SDL.mouseMotionEventPos e }
               SDL.MouseButtonEvent e
                  | SDL.mouseButtonEventButton e == SDL.ButtonLeft && SDL.mouseButtonEventMotion e == SDL.Pressed  -> Last $ Just $ \st ->
                             let st2 = st { mousePressed = True  } in
                             if circleDrawing st then st2 { circlePointA = Just $ SDL.mouseButtonEventPos e, circlePointB = Nothing } else st2
                  | SDL.mouseButtonEventButton e == SDL.ButtonLeft && SDL.mouseButtonEventMotion e == SDL.Released -> Last $ Just $ \st ->
                             let st2 = st { mousePressed = False } in
                             case (isJust $ lastCircle st, lastMultTest st) of
                               (False,Nothing) -> st2
                               (_,Just mt)  -> st2 { lastMultTest = Nothing, multTests = mt : multTests st2, circlePointA = Nothing }
                               (True,_)  -> st2 { lastCircle = Nothing, circles = fromJust (lastCircle st2) : circles st2, circlePointA = Nothing }
                               _ -> st2
               SDL.KeyboardEvent e -> case (SDL.keyboardEventKeyMotion e == SDL.Pressed, SDL.keysymKeycode $ SDL.keyboardEventKeysym e) of
                        (True,SDL.KeycodeA) -> Last $ Just $ \st -> st { centricProjection = not $ centricProjection st, scalePnt = -2 }
                        (True,SDL.KeycodeW) -> Last $ Just $ \st -> st { heptaProjection = not $ heptaProjection st, scalePnt = -3 }
                        (True,SDL.KeycodeC) -> Last $ Just $ \st -> st { circleDrawing = not $ circleDrawing st }
                        (True,SDL.KeycodeF) -> Last $ Just $ \st -> st { multTest = not $ multTest st }
                        (True,SDL.KeycodeQ) -> Last $ Just $ \st -> st { tran = SQ 1 0 0 0, tranB = SQ 1 0 0 0 }
                        (True,SDL.KeycodeD) -> Last $ Just $ \st -> st { dumpDrawings = True }
                        (True,SDL.KeycodeL) -> Last $ Just $ \st -> st { loadDrawings = True }
                        (True,SDL.KeycodeK) -> Last $ Just $ \st -> st { drawPoints = [], circles = [], cirCol = [[]], multTests = [] }
                        (True,SDL.KeycodeJ) -> Last $ Just $ \st -> st { circles = tail $ circles st }
                        (True,SDL.KeycodeH) -> Last $ Just $ \st
                            -> st { circles = head $ cirCol st, cirCol = tail (cirCol st) ++ [circles st]
                                      ++ if isJust (find null $ cirCol st) || null (circles st) then [] else [[]] }
                        (True,SDL.KeycodeU) -> Last $ Just $ \st -> st { projU = not $ projU st }
                        (True,SDL.KeycodeZ) -> Last $ Just $ \st -> if zoom st then st { zoom = False, tran = fromJust (savedTran st) }
                                                                               else st { zoom = True, savedTran = Just (tran st) }
                        (True,SDL.KeycodeP) -> Last $ Just $ \st -> st { scalePnt = scalePnt st * 2 + 0.1 }
                        (True,SDL.KeycodeO) -> Last $ Just $ \st -> st { scalePnt = 0 }
                        (True,SDL.KeycodeI) -> Last $ Just $ \st -> st { scalePnt = -1 }
                        (True,SDL.KeycodeM) -> Last $ Just $ \st -> st { tranB = tranB st * (Q.norm $ SQ 1000 (fz02R st) 0 0) }
                        (True,SDL.KeycodeN) -> Last $ Just $ \st -> st { tranB = tranB st * (Q.norm $ SQ 1000 (fz02L st) 0 0) }
                        (True,SDL.KeycodePageDown) -> Last $ Just $ \st -> st { tranB = tranB st * (Q.norm $ SQ 1000 (fz01R st) 0 0) }
                        (True,SDL.KeycodePageUp) -> Last $ Just $ \st -> st { tranB = tranB st * (Q.norm $ SQ 1000 (fz01L st) 0 0) }
                        (True,SDL.KeycodeUp) -> Last (Just (\st -> st { tranB = tranB st * (Q.norm $ SQ 1000 0 (fm01L st) 0) }))
                        (True,SDL.KeycodeDown) -> Last (Just (\st -> st { tranB = tranB st * (Q.norm $ SQ 1000 0 (fm01R st) 0) }))
                        (True,SDL.KeycodeLeft) -> Last (Just (\st -> st { tranB = tranB st * (Q.norm $ SQ 1000  0 0 (-20) )}))
                        (True,SDL.KeycodeRight) -> Last (Just (\st -> st { tranB = tranB st * (Q.norm $ SQ 1000 0 0 (20) )}))
                        _                    -> mempty
               _ -> mempty
  let change = fromMaybe id $ fmap (\a -> a . (\st -> st { redraw = True })) $ getLast $ foldMap ff events

  let state123 = change $ funTra state0
  state <- sysFun state123

  --print $ length $ drawPoints state
 
  tic <- getTicks
  SDL.rendererDrawColor ren $= V4 0 80 60 255
  ntim <- case (tic - lastt state) > 50 || redraw state of
           True  -> do SDL.present ren 
                       SDL.clear ren
                       --print tic
                       return tic
           False -> return $ lastt state
  
  --SDL.rendererDrawColor ren $= V4 0 80 60 255
  --SDL.rendererDrawColor ren $= V4 0 0 0 255
  --(if redraw state then SDL.clear ren else return ())
  --let fun = (\x -> ((Q.norm $ SQ 1000 0 (p01 state) 0) ^ 10) * x)
  let fun = (\x -> tran state * trim state x )
  let flex = ( sin( realToFrac tic / 500 ) + 1 )
  state2 <- drawSurfaceView flex state ren fun
  --SDL.rendererDrawColor ren $= V4 0 0 0 255

  -- threadDelay 500
  -- print "kuku"
  loop ren (state2 { redraw = False, lastt = ntim })

trim st o@(SQ a b c d)
 | not (heptaProjection st) = o
 | otherwise = o

drawSurfaceView flex state ren fun = do
  Just (SDL.Rectangle (P (V2 vpx vpy)) (V2 vpw0 vph)) <- get $ SDL.rendererViewport ren
  let vpw = vph
  let vpx0 = 0 -- (vpw0-vph)`div`2
  let vpx1 = toEnum $ fromEnum vpx0
  let vpx2 = toEnum $ fromEnum vpx0
  let fw a = (+vpx1) $ round $ realToFrac vpw * ((a+1)/2)
  let fh a = round $ realToFrac vph * ((a+1)/2)
  let wf a = (realToFrac (a-vpx2) / realToFrac vpw) * 2 - 1
  let hf a = (realToFrac (a) / realToFrac vph) * 2 - 1
  let pnt = scalePnt state
  SDL.rendererDrawColor ren $= V4 (0+50) (80+50) (60+50) 255
  -- mapM_ (SDL.drawPoint ren) $ map (fmap (toEnum.fromEnum)) $ drawOnRen state
  let draw = filter (\(a,b) -> sqrt(a*a+b*b) < 0.9) $ map (\(P (V2 a b)) -> (wf a,hf b)) $ drawOnRen state
  let draw2 = map (pointcareDiskToHyperbola3dPnt pnt) draw
  let draw3 = map Q.norm $ map (\a -> (SQ 1 0 0 0 / tran state) * a) $ map Q.norm $ map (\(b,c,d) -> SQ 0 d b c) draw2
  let state2 = if mousePressed state then state { drawPoints = addToLine draw3 $ drawPoints state } else state { drawPoints = addLine draw3 $ drawPoints state }
  let draw4 = map (map ((\[x,y] -> P (V2 (fw (x)) (fh (y))) ).(sqToDisk3 pnt).rotNorm.fun)) $ drawPoints state2
  let flex2 = 1.0
  let draw5 = map (map ((\[x,y] -> P (V2 (fw (x)) (fh (y))) ).(sqToDisk2 140 3 250 flex2).rotNorm.fun)) $ drawPoints state2
  let draw6 = map (map ((\[x,y] -> P (V2 (fw (x)) (fh (y))) ).(sqToDisk2 6.5 4 50 flex2).rotNorm.fun)) $ drawPoints state2
  let draw7 = map (map ((\[x,y] -> P (V2 (fw (x)) (fh (y))) ).(sqToDisk2 1 16 1.25 flex2).rotNorm.fun)) $ drawPoints state2
  let draw8 = map (map ((\[x,y] -> P (V2 (fw (x)) (fh (y))) ).((\[x,y] -> [x-0.5,y]).sqToDisk2 (140) 1 20000 flex2).rotNorm.fun)) $ drawPoints state2
  let draw9 = map (map ((\[x,y] -> P (V2 (fw (x)) (fh (y))) ).((\[x,y] -> [x-1.2,y]).sqToDisk2 (140) 1 2000000 flex2).rotNorm.fun)) $ drawPoints state2
  let drawA = map (map ((\[x,y] -> P (V2 (fw (x)) (fh (y))) ).((\[x,y] -> [x-1.9,y]).sqToDisk2 (140) 1 200000000 flex2).rotNorm.fun)) $ drawPoints state2
  let draw4A = map (map ((\([x,y],v) -> (P (V2 (fw (x)) (fh (y))),v) ).(sqToPlane).rotNorm.fun)) $ drawPoints state2
  case (not $ zoom state, projU state) of
   (_,True) -> do
       SDL.rendererDrawColor ren $= V4 255 255 255 255
       100 `emp` drawCircle ren fw fh
       SDL.rendererDrawColor ren $= V4 255 0 255 255
       mapM_ (mapM_ (uncurry (SDL.drawLine ren)) . filter checkLine . toPairs) $ map (map fst) draw4A
       return $ state2 { drawOnRen = [], maxX = foldr1 max $ filter (not.isNaN) $ concatMap (map snd) draw4A }
   (True,_) -> do
       SDL.rendererDrawColor ren $= V4 255 255 255 255
       100 `emp` drawCircle ren fw fh
       --(ren,2000) `empPoints` drawOnSurfaceV pnt fw fh fun
       --SDL.rendererDrawColor ren $= V4 (0+50) (80+50) (60+50) 255
       SDL.rendererDrawColor ren $= V4 255 255 255 255
       --9000 `emp` drawOnSurface pnt ren fw fh fun
       SDL.rendererDrawColor ren $= V4 255 0 255 255
       mapM_ (mapM_ (uncurry (SDL.drawLine ren)) . filter checkLine . toPairs) draw4
       mapM_ (mapM_ (uncurry (SDL.drawLine ren)) . filter checkLine . toPairs) $ filter (all checkPoint) draw5
       mapM_ (mapM_ (uncurry (SDL.drawLine ren)) . filter checkLine . toPairs) $ filter (all checkPoint) draw6
       mapM_ (mapM_ (uncurry (SDL.drawLine ren)) . filter checkLine . toPairs) $ filter (all checkPoint) draw7
       --mapM_ (SDL.drawPoint ren) $ concat draw4
       SDL.rendererDrawColor ren $= V4 0 80 60 255
       -- 1000 `emp` drawCircle2 ren fw fh
       state3 <- drawCircles fun state2 ren fw fh wf hf
       state4 <- drawMultTest fun state3 ren fw fh wf hf
       return $ state4 { drawOnRen = [] }
   (False,_) -> do
       SDL.rendererDrawColor ren $= V4 255 0 255 255
       mapM_ (mapM_ (uncurry (SDL.drawLine ren)) . filter checkLine . toPairs) $ filter (all checkPoint) draw5
       mapM_ (mapM_ (uncurry (SDL.drawLine ren)) . filter checkLine . toPairs) $ filter (all checkPoint) draw8
       mapM_ (mapM_ (uncurry (SDL.drawLine ren)) . filter checkLine . toPairs) $ filter (all checkPoint) draw9
       mapM_ (mapM_ (uncurry (SDL.drawLine ren)) . filter checkLine . toPairs) $ filter (all checkPoint) drawA
       return state2 { drawOnRen = [] }

drawMultTest fun st ren fw fh wf hf = do
  let drawMT (pntASq,pntBSq) = do
               SDL.rendererDrawColor ren $= V4 255 0 0 255
               mapM_ (uncurry (SDL.drawLine ren)) $ filter checkLine $ toPairs $ fpxline 0.000001 Nothing
                       ( rotn (fun pntASq,fun pntBSq) ) 0
               SDL.rendererDrawColor ren $= V4 0 255 0 255
               mapM_ (uncurry (SDL.drawLine ren)) $ filter checkLine $ toPairs $ fpxline 0.000001 Nothing
                       ( rotn (fun pntASq,fun $ pntBSq * pntASq) ) 0
               SDL.rendererDrawColor ren $= V4 0 0 255 255
               mapM_ (uncurry (SDL.drawLine ren)) $ filter checkLine $ toPairs $ fpxline 0.000001 Nothing
                       ( rotn (fun pntBSq,fun $ pntBSq * pntASq) ) 0
  st2 <- if multTest st && circleDrawing st && isJust (circlePointA st) && isJust (circlePointB st)
             then do
               drawMT (pntASq,pntBSq)
               return ( st { lastMultTest = Just (pntASq,pntBSq) } )
             else do
               return st
  mapM_ drawMT $ multTests st
  return st2
 where
  rotn (a,b) = (Q.rotNorm a,Q.rotNorm b)
  funt (a,b) = (fun a,fun b)
  pnt = scalePnt st
  pxToF (P (V2 a b)) = (wf a, hf b)
  toSQ disk = Q.norm $ (\a -> (SQ 1 0 0 0 / tran st) * a ) $ Q.norm $ (\(b,c,d) -> SQ 0 d b c) $ pointcareDiskToHyperbola3dPnt pnt disk
  pntASq = toSQ $ pxToF $ fromJust $ circlePointA st
  pntBSq = toSQ $ pxToF $ fromJust $ circlePointB st
  fpntASq = fun pntASq
  fpntBSq = fun pntBSq
  sqToPx sq = (\[x,y] -> P (V2 (fw x) (fh y))) $ (sqToDisk3 pnt) $ sq 
  fpxline _ _ _ t | abs(t-0.5) > 3 = []
  fpxline step val (a,b) t = (sqToPx nrm) : (take detail $ fpxline nstep (Just value) (nrm,b) nstep)
    where
     t2 = SQ t 0 0 0
     t1 = SQ (1-t) 0 0 0
     ((e,bb),nrm) = circleNorm (a * t1 + b * t2)
     nns = step * 0.5 + nstep * 0.5
     nstep | isJust dif && (not $ isNaN nv) && (not $ isInfinite nv) = nv
           | otherwise = step
     nv = (min 0.7 (step / (dfv/horiz)))
     horiz = 0.025 / asinh bb
     detail = 200
     value@[x1,y1] = sqToDisk3 pnt nrm
     dfv = fromJust dif
     dif = case val of
             Nothing -> Nothing
             Just [x2,y2] -> if sqrt(x2*x2+y2*y2) < 99 then Just $ sqrt((x1-x2)**2+(y1-y2)**2) else Nothing

drawCircles fun st ren fw fh wf hf = do
  SDL.rendererDrawColor ren $= V4 255 255 255 255
  let expa (a,b) = (expand (a,b) 1, expand (b,a) 1)
  st2 <- if circleDrawing st && isJust (circlePointA st) && isJust (circlePointB st) && not (multTest st)
                then do -- SDL.fillRect ren (Just $ SDL.Rectangle (minus pntAPx) (V2 4 4))
                        -- SDL.fillRect ren (Just $ SDL.Rectangle (minus pntBPx) (V2 4 4))
                        --mapM_ (uncurry (SDL.drawLine ren)) $ filter checkLine $ toPairs $ pxline (pntASq,pntBSq)
                        mapM_ (uncurry (SDL.drawLine ren)) $ filter checkLine $ toPairs $ fpxline 0.000001 Nothing (Q.rotNorm $ fun pntASq,Q.rotNorm $ fun pntBSq) 0
                        return $ st { lastCircle = Just (pntASq,pntBSq) }
                else do
                   return st
  let le = 0
  let rn (a,b) = (Q.rotNorm $ fun b,Q.rotNorm $ fun a)
  let drawL pq = do --t <- randomRIO (-9,1)
                    tic <- getTicks
                    let ftic = sin(realToFrac tic / 500)
                    let t = ftic * 0.01
                    -- let t = (-9)
                    let pq2 = (\(a,b) -> (Q.rotNorm b,Q.rotNorm a)) pq
                    let qp  = (\(a,b) -> (b,a)) pq2
                    -- print $ length $ fpxline 0.000001 Nothing (expa pq2) (0.0)
                    mapM_ (SDL.drawPoint ren) $ fpxline 0.000001 Nothing (expa pq2) (0.0)
                    --mapM_ (SDL.drawPoint ren) $ fpxline 0.001 Nothing qp (0.0)
                    --mapM_ (SDL.drawPoint ren) $ fpxline 0.001 Nothing pq (t+9)
                    --mapM_ (SDL.drawPoint ren) $ fpxline 0.001 Nothing pq (t-9)
  --mapM_ (\pq -> mapM_ (uncurry (SDL.drawLine ren)) $ filter checkLine $ toPairs $ pxline pq) $ circles st
  --mapM_ (\pq -> mapM_ (uncurry (SDL.drawLine ren)) $ filter checkLine $ toPairs $ fpxline 0.000001 Nothing (rn pq) 0) $ circles st
  let cir = map (\(a,b) -> (fun a, fun b)) $ circles st
  mapM drawL cir
  return st2
 where
  expand (a,b) m = snd $ circleNorm (a * t1 + b * t2)
   where
     t = m
     t2 = SQ t 0 0 0
     t1 = SQ (1-t) 0 0 0
  pnt = scalePnt st
  minus (P (V2 a b)) = P (V2 (a-2) (b-2))
  pxToF (P (V2 a b)) = (wf a, hf b)
  toSQ disk = Q.norm $ (\a -> (SQ 1 0 0 0 / tran st) * a ) $ Q.norm $ (\(b,c,d) -> SQ 0 d b c) $ pointcareDiskToHyperbola3dPnt pnt disk
  sqToPx sq = (\[x,y] -> P (V2 (fw x) (fh y))) $ (sqToDisk3 pnt) $ sq 
  pntASq = toSQ $ pxToF $ fromJust $ circlePointA st
  pntBSq = toSQ $ pxToF $ fromJust $ circlePointB st
  pntAPx = sqToPx $ pntASq
  pntBPx = sqToPx $ pntBSq
  tline = map (\t -> (SQ ((t)) 0 0 0,SQ ((1-t)) 0 0 0)) $ map (/200) [(0-200*0)..(200+200*0)]
  hline (a,b) = map (\(t1,t2) -> snd $ circleNorm (Q.rotNorm a * t1 + Q.rotNorm b * t2)  ) tline
  pxline (a,b) = map sqToPx $ hline (fun a, fun b)

  fpxline _ _ _ t | abs(t-0.5) > 3 = []
  fpxline step val (a,b) t = (sqToPx nrm) : (take detail $ fpxline nstep (Just value) (nrm,b) nstep)
    where
     t2 = SQ t 0 0 0
     t1 = SQ (1-t) 0 0 0
     ((e,bb),nrm) = circleNorm (a * t1 + b * t2)
     --t3 = t + 0.0000001
     --nstep = min 0.5 (fe step)
     --fe x = 1/exp(1/x)
     --fe x = x / 1.001
     nns = step * 0.5 + nstep * 0.5
     nstep | isJust dif && (not $ isNaN nv) && (not $ isInfinite nv) = nv
           | otherwise = step

     --nv = (min 0.7 (step / (dfv/0.010)))
     nv = (min 0.7 (step / (dfv/horiz)))
     horiz = 0.025 / asinh bb
     detail = 200

{-
     detail = round $ 200 / (min 1 $ asinh mm)
     mm = (\a -> a-dd) $ foldr1 min [i1,i2,bb]
     --nv = step
     dd = sqrt( (j1-j2)**2 + (k1-k2)**2 )
-}

     value@[x1,y1] = sqToDisk3 pnt nrm
     dfv = fromJust dif
     dif = case val of
             Nothing -> Nothing
             Just [x2,y2] -> if sqrt(x2*x2+y2*y2) < 99 then Just $ sqrt((x1-x2)**2+(y1-y2)**2) else Nothing

     (SQ w1 i1 j1 k1) = Q.rotNorm a
     (SQ w2 i2 j2 k2) = Q.rotNorm b

{-

     (SQ w1 i1 j1 k1) = Q.rotNorm a
     (SQ w2 i2 j2 k2) = Q.rotNorm b

     (_,t0) = fromJust val
     t3 | difM < 4 && difM > 0.25 = t + 0.1
        | isJust val = t0 + 0.1/difM
        | otherwise  = t + 0.1

     difM = case (dif, fromJust dif) of
                      (Just _, a) -> 0.005 / a
                      _ -> 1
-}

{-
     difM = case (dif0,dif) of
              (Just a, Just b) -> if isNaN a || isNaN b || b < 0.0001 || a < 0.0001 then  1
                   else min 50 $ (max 1 (b/a)) * 100
              (Nothing,_) -> 1
              (_,Nothing) -> 1
-}
     -- d = sqrt(x*x+y*y)
     --
{-
     --t3 = t + ((sin (bb*999) + 1)*0.1)/(max 1 e)
     i3 = max i1 i2
     --dif = max 1 $ abs ( i3 - bb )
     --i3 = sinh $ ( asinh i1 + asinh i2 ) / 2
     t3 = t + max 0.0001 (0.05/(max 1 bb)*i3)
-}

{-
     d = sqrt( (j1-j2)**2 + (k1-k2)**2 )
     ai1 = asinh i1
     ai2 = asinh i2
     ai3 = ( ai1 + ai2 ) / 2
     ai4 = ai3 / (ai3 * d + 1)
     ai5 = ai1 - ai3
     e = ai4*(1-(t-0.5)*2) + ai2*(t-0.5)*2
     g = ai2*t*2 + ai1*(0.5-t)*2
     h = abs $ ai1-ai3
     l = abs $ ai2-ai3
     k = h + l
     m = abs (sinh e - sinh ai3)
     n = abs (sinh g - sinh ai3)
     p = m - n
     q = p/2
     r = (p-q) / k
     t = r
-}


circleNorm (SQ a b c d) = ((e,b/e),SQ 0 (b/e) (c/e) (d/e))
 where
  e = sqrt (b*b - c*c - d*d)

 
checkPoint (P (V2 a b)) = not (a > 9999 || b > 9999)

checkLine (P (V2 a b), P (V2 c d)) = sqrt ( (realToFrac $ a-c)**2 + (realToFrac $ b-d)**2 ) < 200

addToLine a (b:c) = (a ++ b) : c

addLine   a (b:c) = a : simplyfyLine b : c
addLine   a b     = a : b

simplyfyLine [] = []
simplyfyLine [a] = [a]
simplyfyLine [a,b] = [a,b]
simplyfyLine a = a

{-
simplyfyLine list = bz
 where
  a = length list `div` 3
  b = foldr1 (+) (take a $ drop a list) / (SQ (realToFrac a) 0 0 0)
  c = head list
  d = last list
  bz = map (\t0 -> let t1 = SQ t0 0 0 0 in let t2 = SQ (1-t0) 0 0 0 in let t3 = SQ (t0*2) 0 0 0 in
                 t2*t2*c + t3*t2*b + t1*t1*d ) $ map (/500) [0..500]
-}
{-
  bz = map (\t0 -> let t1 = SQ t0 0 0 0 in let t2 = SQ (1-t0) 0 0 0 in let t3 = SQ (t0*2) 0 0 0 in
                 let qq = ( (c*(d * t2 + t1)) / d * (d * t1 + t2)) in
                     qq


                ) $ map (/100) [0..100]
-}



toPairs [] = []
toPairs [a] = []
toPairs (a:b:c) = (a,b) : toPairs (b:c)

emp 0 f = return ()
emp n f = f >> emp (n-1) f

empPoints (ren,n) f | n < 100 = return ()
                    | otherwise = SDL.drawPoints ren $ V.generate n fun
 where
  fun _ = unsafePerformIO f

drawCircle ren fw fh = do
  t <- randomRIO (-pi,pi :: Double)
  rx <- randomRIO (-1,1)
  ry <- randomRIO (-1,1)
  let x = sin t + rx*0.003
  let y = cos t + ry*0.003
  SDL.drawPoint ren (P (V2 (fw x) (fh y)))

drawCircle2 ren fw fh = do
  t <- randomRIO (-pi,pi :: Double)
  rx <- randomRIO (-1,1)
  ry <- randomRIO (-1,1)
  let x = sin t * 1.64 + rx*0.05
  let y = cos t * 1.64 + ry*0.05
  SDL.fillRect ren $ Just (SDL.Rectangle (P (V2 (fw (x) - 32) (fh (y) -32))) (V2 64 64))
  --SDL.drawPoint ren (P (V2 (fw x) (fh y)))

drawOnSurface pnt ren fw fh fun = do
  a <- randomPoincareDisk
  let (b,c,d) = pointcareDiskToHyperbola3d a
  let e = SQ 0 d b c
  let f = fun $ Q.norm e
  let g = rotNorm f
  let [x,y] = sqToDisk3 pnt g
  SDL.drawPoint ren (P (V2 (fw x) (fh y)))

drawOnSurfaceV fw fh fun = do
  a <- randomPoincareDisk
  let (b,c,d) = pointcareDiskToHyperbola3d a
  let e = SQ 0 d b c
  let f = fun $ Q.norm e
  let g = rotNorm f
  let [x,y] = sqToDisk g
  return (P (V2 (fw x) (fw y)))

















