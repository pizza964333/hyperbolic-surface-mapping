
import qualified Data.ByteString as B
import Codec.Image.PBM
import qualified Data.Array.BitArray as BA

main = do
  a <- readFile "hepta.pbm"
  let (Right (b,_)) = decodePlainPBM a
  mapM_ print $ map fst $ filter (snd) $ BA.assocs $ pbmPixels b
