module Data.HyperbolicTree where

import System.Random
import System.IO.Unsafe

type Loc = (Double,Double,Double)

isValidLoc :: Loc -> Bool
isValidLoc (a,b,c) = d == 1 || (d > prec && d < maxB)
 where
  prec = 0.9999999
  maxB = 2-maxB
  d = ( a*a - b*b - c*c )

data Tree a = Leaf (Maybe Loc) a | Fork (Maybe Loc) a (Tree a) (Tree a) | View (Maybe Loc) a (Tree a) (Tree a) (Tree a)
 deriving (Show)

randomTree :: Int -> IO (Tree ())
randomTree 0 = return $ Leaf Nothing ()
randomTree n = do
  a <- randomRIO (0,n-1)
  b <- randomRIO (0,n-1)
  c <- randomRIO (0,n-1)
  d <- unsafeInterleaveIO $ randomFork a
  e <- unsafeInterleaveIO $ randomFork b
  f <- unsafeInterleaveIO $ randomFork c
  return $ View Nothing () d e f

randomFork :: Int -> IO (Tree ())
randomFork 0 = return $ Leaf Nothing ()

randomFork n = do
  a <- randomRIO (0,n-1)
  b <- randomRIO (0,n-1)
  c <- unsafeInterleaveIO $ randomFork a
  d <- unsafeInterleaveIO $ randomFork b
  return $ Fork Nothing () c d


