module Data.SplitQuaternionForHyperbolicSurface where

import Data.List
import System.Random
import System.IO.Unsafe
import Data.Ratio
import qualified Data.Vector.Storable as V
import Data.Quaternion.Tools
import qualified Data.Vec.Base as B
import qualified Data.Vec.LinAlg as L
import qualified Data.Quaternion as Q
import Data.Quaternion (Quat(Q))
import Data.Complex

type Loc = Double

data SQ = SQ { real :: Loc, imagI :: Loc, imagJ :: Loc, imagK :: Loc }
 deriving (Eq,Read,Show)

{-
instance Show SQ where
  show (SQ a b c d) = show $ map realToFrac [a,b,c,d]
-}

instance Fractional SQ where
  a / b = a `divq` b

cut (SQ a b c d) = SQ a 0 c d

toList (SQ a b c d) = [a,b,c,d]

add :: SQ -> SQ -> SQ
add (SQ w1 i1 j1 k1) (SQ w2 i2 j2 k2) = SQ (w1+w2) (i1+i2) (j1+j2) (k1+k2)

mul :: SQ -> SQ -> SQ

mul (SQ w1 i1 j1 k1) (SQ w2 i2 j2 k2) = SQ w i j k
 where
  w = w1*w2 - i1*i2 + j1*j2 + k1*k2
  i = w1*i2 + i1*w2 + j1*k2 - k1*j2
  j = w1*j2 + i1*k2 + j1*w2 - k1*i2
  k = w1*k2 - i1*j2 + j1*i2 + k1*w2

divq :: SQ -> SQ -> SQ

divq (SQ w1 i1 j1 k1) (SQ w2 i2 j2 k2) = SQ (w/p) (i/p) (j/p) (k/p)
 where
  w =   w1*w2  + i1*i2 - j1*j2 - k1*k2
  i = (-w1*i2) + i1*w2 - j1*k2 + k1*j2
  j = (-w1*j2) - i1*k2 + j1*w2 + k1*i2
  k = (-w1*k2) + i1*j2 - j1*i2 + k1*w2
  p = w2*w2 + i2*i2 - j2*j2 - k2*k2


{-
rotNorm2 (SQ w1 i1 j1 k1) = SQ w i j k
 where
  w = w1*(i1/p) - i1*(w1/p) + j1*(k1/q) + k1*(j1/p)
  i = p*4
  j = w1*(k1/q) + i1*(k1/q) + j1*(i1/p) - k1*(w1/p)
  k = w1*(k1/q) - i1*(k1/q) + j1*(w1/p) + k1*(i1/p)
  p = w1*w1 + i1*i1 - j1*j1 - k1*k1
  q = -p
-}

divqFR (SQ w2 i2 j2 k2) = SQ (i2/p) (w2/p) (k2/q) (j2/p)
 where
  w =  i2
  i =  w2 
  j =  -k2 
  k =  j2 
  p = w2*w2 + i2*i2 - j2*j2 - k2*k2
  q = -p



{-
rotNorm2 (SQ w1 i1 j1 k1) = SQ 0 i j k
 where
  p = w1*w1 + i1*i1 - j1*j1 - k1*k1 -- p = 1
  i = w1*w1/p + i1*i1/p + j1*j1/p + k1*k1/p
  j = ( i1*j1/p - w1*k1/p ) * 2
  k = ( j1*w1/p + k1*i1/p ) * 2
-}

rotNorm2 (SQ w1 i1 j1 k1) = SQ 0 i j k
 where
  i = w1*w1 + i1*i1 + j1*j1 + k1*k1
  j = ( i1*j1 - w1*k1 ) * 2
  k = ( j1*w1 + k1*i1 ) * 2

{-

SQ {real = 1.6666666666666667, imagI = 0.0, imagJ = 1.3333333333333333, imagK = 0.0}

moveJ (SQ w2 i2 j2 k2) = SQ w i j k
 where
  w = 1.66*w2 + 1.33*j2
  i = 1.66*i2 + 1.33*k2
  j = 1.66*j2 + 1.33*w2
  k = 1.66*k2 + 1.33*i2

rotNorm2 (SQ (1.3*j2) (1.6*i2+1.3*k2) (1.6*j2) (1.6*k2+1.3*i2)) = SQ 0 i j k
 where
  i = 1.7*j2*j2 + i1*i1 + 2.7*j2*j2 + k1*k1
  j = ( 2.7*i2*j2 + 2.2*k2*j2 - 1.3*j2*k1 ) * 2
  k = ( 2.2*j2 + k1*i1 ) * 2


(1.6*i2+1.3*k2) * (1.6*k2+1.3*i2)
-}

{-

w1*i1/p = -j1*j1 - k1*k1 + w1*w1 - 1

-}



{-
rotNorm a@(SQ w _ _ _) = SQ 0 c d e
 where
  SQ b c d e = a * ( SQ 0 1 0 0 / a )
-}

{-
rotNorm2 a@(SQ w _ _ _) = SQ 0 c d e
 where
  SQ b c d e = a * ( divqFR a )
-}

--rotNorm2 = rotNorm3

instance Num SQ where
  a + b = add a b
  a * b = mul a b

randomSQ = do
  a <- randomRIO (-10,10)
  b <- randomRIO (-10,10)
  c <- randomRIO (-10,10)
  d <- randomRIO (-10,10)
  return $ SQ a b c d

randomSQ2 = do
  a <- randomRIO (-10,10)
  b <- randomRIO (-10,10)
  return $ SQ a 0 b 0

norm o@(SQ a b c d) = o / e
 where
  sq a | a > 0     = SQ (sqrt a) 0 0 0
       | otherwise = SQ 0 (sqrt (-a)) 0 0
  e = sq ( a*a + b*b - c*c - d*d )

list =  map (\_ -> unsafePerformIO $ randomSQ) [0..]

list2 =  map (\_ -> unsafePerformIO $ randomSQ2) [0..]

list3 =  map (\(a,b,c) -> SQ 0 c a b) $ map pointcareDiskToHyperbola3d $ map (\_ -> unsafePerformIO $ randomPoincareDisk) [0..]

list4 =  map norm $ map (\(a,b,c) -> SQ 0 c a b) $ map cylinderToHyperbola3d $ map (\_ -> unsafePerformIO $ randomCylinder) [0..]

mkSq disk = norm $ hyperbolaToSq $ pointcareDiskToHyperbola3d disk

hyperbolaToSq (a,b,c) = SQ 0 c a b

sq n = list !! n

transf f 0 x = []
transf f n x = x ++ transf f (n-1) (map f x)

{-
pointcareDiskToHyperbola3d (x,y) = (d*c,e*c,b)
  where
   a = sqrt(x*x+y*y)
   b = (-a*a-1)/(a*a-1)
   c = sqrt(b*b-1)
   d = x/a
   e = y/a
-}

diskToCubeTest :: Double
diskToCubeTest = check
 where
  [v1,v2,v3] = map j [0,1,2]
  q = [conv $ v1,conv $ rot $ rot v2,conv $ rot v3]
  check = foldr1 (+) $ map dif $ transpose q
  dif [a,b,c] = (a-b)**2 + (a-c)**2 + (b-c)**2
  rot o@(a,b,c) = (b,c,a)
  conv (a,b,c) = [a,b,c]
  j x = diskToCube (sin (f x) * 0.999,cos (f x) * 0.999)
  f x = pi/3*2*x
  diskToCube = hyperbola3dToCubicProjection .  pointcareDiskToHyperbola3d

diskToCube = hyperbola3dToCubicProjection .  pointcareDiskToHyperbola3d

randomCubic = do
  a <- randomPoincareDisk
  --let a = (a1*0.99,a2*0.99)
  let b = pointcareDiskToHyperbola3d a
  let (x,y,z) = hyperbola3dToCubicProjection b
  --mm <- randomRIO (0,1) :: IO Int
  let mm = 0
  --let m = realToFrac mm * 2 - 1
  let d = sqrt 3 / 2
  let xx = -x
  let yy = -y
  let zz = -z
  --return [x,y,z]
  if mm == 0 then if x > 0 && y > 0 && z > 0 then return [x,y,z] else randomCubic
             else if x > 0 && y > 0 && z > 0 then return [xx,yy,zz] else randomCubic

cubicMag [x,y,z] = (x+y+z)**2 - x*x - y*y - z*(z :: Double)

randomCubic2 = do
  x <- randomRIO (-50,50)
  y <- randomRIO (-50,50)
  z <- randomRIO (-50,50)
  let m = cubicMag [x,y,z]
  let n = sqrt $ abs m
  --return [x/n,y/n,z/n]
  -- if m < 0 then randomCubic2 else return [x/n,y/n,z/n]
  --if not $ check (x*x < 0.1) (y*y < 0.1) (z*z < 0.1) then randomCubic2 else return [x/n,y/n,z/n]
  --if m > 0 && chk x y z then return [x/n,y/n,z/n] else randomCubic2
  --if chk (x > 0) (y > 0) (z > 0) then randomCubic2 else return [x/n,y/n,z/n]
  if m > 0 && chk (x > 0) (y > 0) (z > 0) then return [x/n,y/n,z/n] else randomCubic2
 where

  chk True True True = True
  chk False False False = True
  chk _ _ _ = False

  check True False False = False
  check False True False = False
  check False False True = False
  check _ _ _ = True

randomCubic3 = do
  w <- randomRIO (-50,50)
  x <- randomRIO (-50,50)
  y <- randomRIO (-50,50)
  z <- randomRIO (-50,50 :: Double)
  let cubicMag [x,y,z] = (x+y+z)**2 + w*w - x*x - y*y - z*z
         where
           p x = sqrt(x*x+1)
           f x = x**2
  let m = cubicMag [x,y,z]
  let j x = x ** (1/2)
  let n = j m
  if isNaN m || isNaN n || not ( chk (x > 0) (y > 0) (z > 0) )
  --if isNaN m || isNaN n
           then randomCubic3 else return [w/n,x/n,y/n,z/n]
 where
  chk True True True = True
  chk False False False = True
  chk _ _ _ = False
  
  
  
  

hyperbola3dToCubicProjection (u,v,ww) = (x+w,y+w,z+w)
 where
  m = sqrt 3 / 2
  --w = ww/2.5
  w = ww/sqrt 6
  n = 1 / (sqrt 2 * 2)
  d = sqrt(u*u+v*v)
  uu = u/d
  vv = v/d
  --q = Q.normalize $ Q 0 (0.5,0.5,exp 1 / 2)
  q = Q 0.0 (0.32505758367188436,0.32505758367188436,0.8880738339771034)
  --q = Q.normalize $ Q 0 (0.5,0.5,p)
  --q = Q.normalize $ Q 0 (n,n,m)
  --(x B.:. y B.:. z B.:. ()) = rotateQuat q (u B.:. v B.:. 0 B.:. ())
  (x B.:. y B.:. z B.:. ()) = rotateQuat q (u B.:. v B.:. 0 B.:. ())

{-

*Data.SplitQuaternionForHyperbolicSurface> diskToCubeTest 1.36602540378435233
8.556807812852441e-22
*Data.SplitQuaternionForHyperbolicSurface> Q.normalize $ Q 0 (0.5,0.5,1.36602540378435233)
Q {real = 0.0, imag = (0.32505758367188436,0.32505758367188436,0.8880738339771034)}

-}

pointcareDiskToHyperbola3d (x,y) = (xx,yy,zz)
  where
   z = 1
   d = sqrt(z*z-x*x-y*y)
   xx = x/d
   yy = y/d
   zz = z/d

cylinderToHyperbola3d (u,v) = (sin u * aa, cos u * aa, vv)
 where
  a = 1
  d = sqrt(a*a-v*v)
  aa = a/d
  vv = v/d

pointcareDiskToHyperbola3dPnt pnt (x,y) = (xx,yy,zz)
  where
   z = 1
   d = sqrt(z*z-x*x-y*y)
   xx = x/d
   yy = y/d
   zz = z/d

testMg (x,y,z) = z*z - x*x - y*y

{-# NOINLINE loadHeptaGrid #-}
loadHeptaGrid :: V.Vector Double
loadHeptaGrid = unsafePerformIO $ do
  a <- readFile "hepta.txt"
  let b = lines a
  let c = map read b :: [(Double,Double)]
  return $ V.fromList $ concatMap (\(a,b) -> [a,b]) c

randomHepta = do
   a <- randomRIO (0,len-1)
   let b = loadHeptaGrid V.! (a*2)
   let c = loadHeptaGrid V.! (a*2+1)
   let (u,v) = ((b-5)/590-1,(c-5)/590-1)
   let m = 1 -- 1.06
   return (u*m,v*m)
 where
  len = V.length loadHeptaGrid `div` 2

jj n f = writeFile ("/tmp/plot_" ++ show n) $ unlines $ map (unwords . map show . sqToDisk . rotNorm . f . norm) $ take 10000 list3

jjFun = (\x -> ((norm $ SQ 1000 0 20 20) ^ 10) * x)

randomCylinder :: IO (Double,Double)
randomCylinder = do
  a <- randomRIO (-pi,pi)
  b <- randomRIO (-1,1)
  return (a,b)
  

randomPoincareDisk :: IO (Double,Double)
randomPoincareDisk = do
  (a,b) <- randomHepta
  --a <- randomRIO (-1,1)
  --b <- randomRIO (-1,1)
  let dist = sqrt(a*a+b*b)
  --if dist < 1.0 && ( ( sin (b*30) * sin(a*30) ) > 0 ) then return (a,b) else randomPoincareDisk
  if dist < 1.0 then return (a,b) else randomPoincareDisk
{-
  let adist = tanh (dist*n)
  let aa = a/dist*adist*n
  let bb = b/dist*adist*n
  let hdist = atanh $ sqrt (aa*aa+bb*bb)
  --if dist < 1.0 then return (aa,bb) else randomPoincareDisk
  if (dist < 1.0 && ( sin(hdist*90) ) > 0) then return (aa,bb) else randomPoincareDisk
 -}

{-
rotNorm (SQ a b c d) = SQ (dist) 0 (e) (f)
 where
  dist = sqrt ( a*a+b*b )
  (e,f) = rot (a) (b) (c) (d)
  rot a b c d = (c*b-d*a,c*a+d*b)
-}

rotNorm = rotNorm2

rotNorm3 a@(SQ w _ _ _) = SQ 0 c d e
 where
  SQ b c d e = a * ( SQ 0 1 0 0 / a )

rotNormOpt a@(SQ w _ _ _) | w == 0 = a
                          | otherwise = SQ 0 c d e
 where
  SQ b c d e = a * ( SQ 0 1 0 0 / a )

rotNormB a = SQ c 0 d e
 where
  SQ b c d e = a * ( SQ 1 0 0 0 / a )

getM (SQ a b c d) = a*a + b*b - c*c - d*d

--sqToPlane (SQ _ a b c) = [(b - a + aa)/10-0.5,c/10]
{-
sqToPlane (SQ _ a b c) = [(bb+cc)/10,(c)/10]
 where
  bb = asinh b + aa
  cc = asinh c - aa
  aa = asinh (a-1)
-}

sqToPlane (SQ _ a b c)
   | m1 > 0.01 && abs(y) > 0.3 = ([999,999],0)
   | otherwise = ([x,y],d3*m1*m4)
 where
  aaa = a+1
  dist = sqrt(aaa**2+b*b+c*c)/sqrt(2)
  aa = aaa/dist
  bb = b/dist/aa
  cc = c/dist/aa
  x = bb + b3/800 + d3/15*m1
  y = cc + c3/800 + c/15*m1*m3
  d2 = sqrt(b*b+c*c)
  b2 = b/d2
  c2 = c/d2
  d3 = asinh d2
  b3 = b2*d3
  c3 = c2*d3
  m1  = (tanh ((bb - 1.0)*50) + 1 ) / 2
  m2  = (tanh ((d3 - 5.5)*999999) + 1 ) / 2
  m3  = (tanh ((bb - 0.966)*400) + 1 ) / 2
  m4  = (tanh ((bb - 0.966)*9999999) + 1 ) / 2
  mA = m1

{-
  b2 = asinh b  - 4
  m1  = (tanh ((b - 20)/80) + 1 ) / 2
  m2B  = (tanh ((xx - 1.0)*199990) + 1 ) / 2
  m3  = (tanh ((xx - 1.6)*10) + 1 ) / 2
  m4  = (tanh (((0.04)-cc*cc)*50) + 1 ) / 2
  -- m5  = (tanh ( ( (b2/1000) - sqrt((c/10)**2+1) ) / 50 ) + 1 ) / 2
  m5  = (tanh ( ( (1.1) - sqrt((cc + c/10)**2+1) ) * 5990 ) + 1 ) / 2
  xx = bb + b2*m/10
  m = m1 * m2
  -- n = m2 * m1 * m3 * m4 * m5
  n = m1 * m2 * m5
  --n  = (tanh ((1/abs(c))*100) + 1 ) / 2
  -- m = mm*n
-}

sqToDisk (SQ _ a b c) = [bb,cc]
 where
  aaa = a+1
  dist = sqrt(aaa**2+b*b+c*c)/sqrt(2)
  aa = aaa/dist
  bb = b/dist/aa
  cc = c/dist/aa

{-
sqToPlane (SQ _ a b c) = [b3*aa/10,c3*aa/10]
 where
  dist = sqrt(b*b+c*c)
  bb = b/dist
  cc = c/dist
  c2 | bb < 0 = asin cc - pi/2
     | otherwise = asin (-cc) + pi - pi/2
  aa = asinh (a-1)
  a2 = aa**3
  -- t = c(2
  t = asinh(c2 * a2) / asinh (pi*a2) * pi
  c3 = sin ( t )
  b3 = cos ( t )
  -- d = 0 -- 1 - min 1 (10/aa)
  -- d = 0.99 -- ( tanh (  aa / 5 ) + 1 ) / 2.2
-}

sqToDisk (SQ _ a b c) = [bb,cc]
 where
  aaa = a+1
  dist = sqrt(aaa**2+b*b+c*c)/sqrt(2)
  aa = aaa/dist
  bb = b/dist/aa
  cc = c/dist/aa

sqToDisk3 (-2) o = sqToCentric o
sqToDisk3 (-3) o = sqToHepta o
sqToDisk3 point (SQ _ a b c) = [bb,cc]
 where
  aaa = a+1+point
  dist = sqrt(aaa**2+b*b+c*c)/sqrt(2)
  aa = aaa/dist
  bb = b/dist/aa
  cc = c/dist/aa

sqToCentric (SQ _ a b c) = [bb*ad/5,cc*ad/5]
 where
  d = sqrt(b*b+c*c)
  bb = b/d
  cc = c/d
  ad = asinh d

sqToHepta o@(SQ _ a b c) = [u ,v]
 where
  [x,y] = sqToDisk o
  z = x :+ y
  i = 0 :+ 1
  f z = (z-i)/((-z)*i+1)
  j a w = (i*(a+w)+1)/(w+a+i)
  k h w = exp (i*h) * w
  r p w = (p*i*w+1)/(p*w+i)
  (u :+ v) =  ff z

  --ff z =   md ( (atanh $ rot7A $ tanh $ md $ atanh z) - 0.21 )
  --
  ff z =  f z

  md (a :+ b) = atan ( tan (a * 6 ) ) / 6 :+ b
  md2 (a :+ b) = (a + 0.13*signum b) :+ (abs b)

  rot7A z = k (pi/2) z
  trim z = tf $ (z**7) ** (1/7)
   where
    tf (a :+ b) = a :+ abs b

{-
  mir z = (z**14) ** (1/14)
  rot2 z = k (-(pi/2)) z

   where
    a = trim z
-}
{-
   where
    a = k (-(pi/2)) z
    b = (a**7) ** (1/7)
    c = k ((pi/2)) b
-}

sqToDisk2 pl dv scale flex (SQ _ a b c) = [b3+mi,c3]
--sqToDisk2 flex (SQ _ a b c) | dd2 > 1.5 = [999,999]
--                            | otherwise  = [b3,c3]
 where
  aaa = a-1
  dist = sqrt(aaa**2+b*b+c*c)/sqrt(2)
  aa = aaa/dist
  bb = b/dist/aa
  cc = c/dist/aa
  dd = sqrt $ bb*bb+cc*cc
  b2 = bb/dd
  c2 = cc/dd
  mi = 1 - 1.25 / scale
  b3 = (b2-mi) * dd2
  c3 = (c2) * dd2
  dd2 = tanh ( log ( dd - 1 )/8 + flex  )*(scale/dv) + (scale/dv) + pl

{-
  b3 = (b2-0.995) * dd2
  c3 = (c2) * dd2
  dd2 = tanh ( log ( dd - 1 )/8 + flex  )*250 + 251
-}

{-
sqToDisk (SQ _ a b c) = [bb,cc]
 where
  aaa = a+1
  dist = sqrt(aaa**2+b*b+c*c)/sqrt(2)
  aa = aaa/dist
  bb = b/dist/aa
  cc = c/dist/aa
-}

{-
sqToDisk (SQ test a c d)
  | test /= 0 = error "must be rotation normalized"
  | otherwise = [cc*e,dd*e]
 where
  aa = a
  e = sqrt(aa-1)/sqrt(aa+1)
  f = sqrt(c*c+d*d)
  cc = c/f
  dd = d/f
-}


{-
  dist = sqrt((a)**2+c*c+d*d)
  aa = (a)/dist
  cc = c/dist/aa
  dd = d/dist/aa
-}

{-
  d2 = sqrt(cc*cc+dd*dd)
  ccc = cc / d2
  ddd = dd / d2

  e = d2 / 0.79
-}





