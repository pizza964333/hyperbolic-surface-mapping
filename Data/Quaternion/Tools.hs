module Data.Quaternion.Tools where

import Data.Vec.Base
import Data.Vec.LinAlg
import qualified Data.Quaternion as Q
import Data.Quaternion (Quat(Q))

rotateQuat :: Num a => Q.Quat a -> Vec3 a -> Vec3 a
rotateQuat (Q.Q qw (qx,qy,qz)) i@(ix :. iy :. iz :. ()) = o
 where
  a = cross i (qx :. qy :. qz :. ())
  b = a + ((ix*qw) :. (iy*qw) :. (iz*qw) :. ())
  (mx :. my :. mz :. ())    = cross b (qx :. qy :. qz :. ())
  o@(ox :. oy :. oz :. ())  = i + ((mx*2) :. (my*2) :. (mz*2) :. ())

quatToVec3 :: Num a => Q.Quat a -> Vec3 a
quatToVec3 q = rotateQuat q (0 :. 0 :. 1 :. ())

